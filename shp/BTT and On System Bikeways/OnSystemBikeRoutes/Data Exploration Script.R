library(varhandle)
library(magrittr)
library(sf)

txdot_projects <- st_read("TxDOT_Projects_Layer.shp")

colnames(txdot_projects)

unique(txdot_projects$TYPE_OF_WO)
plot(x=txdot_projects$NBR_LET_YE, y=txdot_projects$CONSTRUCTI, main = "Construction Projects by Cost and Let Year", xlab = "Let Year", ylab = "Estimated Project Cost")


#plot(x, y, main="title", sub="subtitle",
#xlab="X-axis label", ylab="y-axix label",
#xlim=c(xmin, xmax), ylim=c(ymin, ymax))

texas_bikeways <- st_read("OnSystemBikeRoutes_20150831.shp")